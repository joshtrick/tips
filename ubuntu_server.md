## Disable sleep of the system
```
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
```

## Auto mount another disk
### Back up fstab
```
sudo cp /etc/fstab /etc/fstab.bak
```

### Check disk uuid
```
blkid
```

### Add disk setting to /etc/fstab

```
sudo mkdir -p /mnt/abcd
echo "/dev/disk/by-uuid/12345 /mnt/abcd ext4 defaults 0 0" | sudo tee -a /etc/fstab
ln -s /mnt/abcd ~/abcd
```

## Fix nvidia-docker gpg issue (20.04)
### Configure hosts
```
sudo cp /etc/hosts /etc/hosts.bak
echo "185.199.108.153 nvidia.github.io" | sudo tee -a /etc/hosts
echo "185.199.109.153 nvidia.github.io" | sudo tee -a /etc/hosts
echo "185.199.110.153 nvidia.github.io" | sudo tee -a /etc/hosts
echo "185.199.111.153 nvidia.github.io" | sudo tee -a /etc/hosts
```

### Change apt sources list
Edit /etc/apt/sources.list.d/nvidia-docker.list - replace 18.04 with 20.04

## Change docker storage location
```
sudo cp /etc/docker/daemon.json /etc/docker/daemon.json.bak
mkdir -p data/root/path
```
Add "data-root": "data/root/path" to /etc/docker/daemon.json
