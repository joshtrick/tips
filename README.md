# Tips

## Table of Contents
- [Run RPI GUI via ssh](run_rpi_gui_via_ssh.sh)
- [chmod](chmod.sh)
- [Docker Display from Mac](docker_display_from_mac.md)
- [Add Fake Display for Headless Mode](add_fake_display_for_headless_mode.md)
- [Ubuntu Server](ubuntu_server.md)
- [Jetson Setup Headless VNC](jetson_setup_headless_vnc.md)
- [RPI Static IP](rpi_static_ip.md)
