# set auto-login
set auto-login during OS installation

# set Vino
```
gsettings set org.gnome.Vino prompt-enabled false
gsettings set org.gnome.Vino require-encryption false
```

# set headless display
```
sudo vim /etc/X11/xorg.conf
```

```
Section "Screen"
   Identifier    "Default Screen"
   Monitor       "Configured Monitor"
   Device        "Default Device"
   SubSection "Display"
       Depth     24
       Virtual   1920 1080
   EndSubSection
EndSection
```

# run vnc server
```
export DISPLAY=:0
/usr/lib/vino/vino-server
```
