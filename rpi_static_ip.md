```
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.bak
sudo vim /etc/dhcpcd.conf
```

```
# Example static IP configuration:
interface eth0
static ip_address=10.0.0.3/24
#static ip6_address=fd51:42f8:caae:d92e::ff/64
#static routers=192.168.0.1
#static domain_name_servers=192.168.0.1 8.8.8.8 fd51:42f8:caae:d92e::1
```
